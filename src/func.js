const getSum = (...nums) => {
  if (!nums.every(x => typeof x === 'string') || nums.every(x => isNaN(x))) {
    return false
  } else {
    return nums.reduce((prev, cur) => Math.floor(prev) + Math.floor(cur), 0).toString()
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = 0;
  let commentsCount = 0;
  listOfPosts.forEach((post) => {
    if (authorName === post.author) {
      postsCount++;
    }
    if (post.hasOwnProperty('comments')) {
      post.comments.forEach((comment) => {
        if (authorName === comment.author) {
          commentsCount++;
        }
      })
    }
  })
  return `Post:${postsCount},comments:${commentsCount}`
};

const tickets = (people) => {
  let count = 0;
  let res = 'YES';
  people.forEach((el) => {
    if ((el - 25) > count) {
      res = 'NO';
    }
    count += el - (el - 25);
  })

  return res
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
